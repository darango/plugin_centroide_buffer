# encoding: utf-8
# -----------------------------------------------------------
# Copyright (C) 2018 Matthias Kuhn
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

from qgis.core import QgsProject  #importa proyecto qgis
from PyQt5 import uic #importa diálogo creado con qtdesigner
import os 
from qgis.core import QgsVectorLayer  

#cargo archivo de diálogo creado con qtdesigner
DialogBase, DialogType = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'centroide_dialogo.ui'))


class GeometryOperationDialog(DialogType, DialogBase): #clase que se importa al archivo principal
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.loadLayers()

        self.bufferButton.clicked.connect(self.buffer) #para ejecutar la función buffer cuando se presiones el pushbutton buffer
        self.centroidButton.clicked.connect(self.centroid)

    def loadLayers(self):
        """
        Carga todas las capas del proyecto y las muestra en el combobox
        """
        for layer in QgsProject.instance().mapLayers().values():
            self.layerComboBox.addItem(layer.name(), layer)

    def buffer(self):
        """
        Genero un buffer sobre la capa seleccionada en el combobox.
        Almaceno el buffer creado en una capa de memoria temporal.
        """

        # Cojo la capa seleccionada en el combobox
        layer = self.layerComboBox.currentData() 
        # Cojo el valor del tamaño de buffer seleccionado antes
        buffer_size = self.bufferSize.value()

        # Creo la capa y abro la edición para poder editar cambios en ella
        #defino geometría, codigo crs (authid mantiene el crs de la capa original)
        uri = "Polygon?crs={authid}&index=yes".format(authid=layer.crs().authid()) #ver uri en api qgis
        layer_name = "Buffer ({layername})".format(layername=layer.name())
        new_layer = QgsVectorLayer(uri, layer_name, "memory")# parametros: uri (depende del provider),mombre capa, provider)

        new_layer.startEditing()

        # Añado todos los atributos de la capa 
        for field in layer.fields():
            new_layer.addAttribute(field)

        # Proceso todos los atributos
        for feature in layer.getFeatures():
            # creo el buffer
            geom = feature.geometry().buffer(buffer_size, 5)

            feature.setGeometry(geom)
            new_layer.addFeature(feature)

        # guardo los cambios en la capa y añado la capa al mapa
        new_layer.commitChanges()

        QgsProject.instance().addMapLayer(new_layer)

    def centroid(self):
        """
        Crea un centroide para todos los poligonos de una capa de polígonos seleccionada.
        Almaceno el resultado en una capa de memoria temporal.
        """

        # Cojo la capa seleccionada en el combobox
        layer = self.layerComboBox.currentData()

        # Creo una capa
        uri = "Point?crs={authid}&index=yes".format(authid=layer.crs().authid())
        layer_name = "Centroid ({layername})".format(layername=layer.name())
        new_layer = QgsVectorLayer(uri, layer_name, "memory")

        new_layer.startEditing()

        # Añado los atributos
        for field in layer.fields():
            new_layer.addAttribute(field)

        # Proceso todos los features
        for feature in layer.getFeatures():
            # Here is the important call: create a centroid
            geom = feature.geometry().centroid()

            feature.setGeometry(geom)
            new_layer.addFeature(feature)

        # guardo los cambios en una capa de memoria y cargo los resultados en el mapa
        new_layer.commitChanges()

        QgsProject.instance().addMapLayer(new_layer)
